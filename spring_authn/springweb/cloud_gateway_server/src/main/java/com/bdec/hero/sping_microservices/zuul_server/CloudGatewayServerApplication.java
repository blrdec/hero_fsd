package com.bdec.hero.sping_microservices.zuul_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CloudGatewayServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CloudGatewayServerApplication.class, args);
	}

}
