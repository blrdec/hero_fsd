package com.bdec.hero.spring_web.spring_security_oauth;

public enum Provider {
	LOCAL, GOOGLE, FACEBOOK, GITHUB
}
